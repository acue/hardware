PCBNEW-LibModule-V1  vie 17 oct 2014 11:05:56 CEST
# encoding utf-8
Units mm
$INDEX
CC4V-T1A
HC-06
WL-SFTW-SMD
$EndINDEX
$MODULE CC4V-T1A
Po 0 0 0 15 543E4106 00000000 ~~
Li CC4V-T1A
Sc 0
AR /53C39F3E
Op 0 0 0
T0 3.81 0 1 1 900 0.15 N V 21 N "X1"
T1 0 1.905 0.7 0.7 0 0.15 N V 21 N "32.768KHz"
DS 0.762 0 0.508 0 0.15 21
DS 0.508 -1.27 0.508 1.27 0.15 21
DS -0.254 -1.27 -0.254 1.27 0.15 21
DS -0.254 1.27 0.254 1.27 0.15 21
DS 0.254 1.27 0.254 -1.27 0.15 21
DS 0.254 -1.27 -0.254 -1.27 0.15 21
DS -0.508 0 -0.508 1.27 0.15 21
DS -0.762 0 -0.508 0 0.15 21
DS -0.508 0 -0.508 -1.27 0.15 21
DS 0.762 0 1.016 0 0.15 21
DS -1.016 0 -0.762 0 0.15 21
$PAD
Sh "2" R 1.3 2.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2.1 0
$EndPAD
$PAD
Sh "1" R 1.3 2.2 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.1 0
$EndPAD
$EndMODULE CC4V-T1A
$MODULE HC-06
Po 0 0 0 15 5440DBED 00000000 ~~
Li HC-06
Sc 0
AR 
Op 0 0 0
T0 0 -16.5 1 1 0 0.15 N V 21 N "HC-06"
T1 0 14 1 1 0 0.15 N V 21 N "VAL**"
DS 5 8.5 5 11 0.15 21
DS -5.5 -11 -5.5 -8.5 0.15 21
DS 4 -13 4 -14.5 0.15 21
DS 4 -14.5 3 -14.5 0.15 21
DS 3 -14.5 3 -13 0.15 21
DS 3 -13 2 -13 0.15 21
DS 2 -13 2 -14.5 0.15 21
DS 2 -14.5 1 -14.5 0.15 21
DS 1 -14.5 1 -13 0.15 21
DS 1 -13 0 -13 0.15 21
DS 0 -13 0 -14.5 0.15 21
DS 0 -14.5 -1 -14.5 0.15 21
DS -1 -14.5 -1 -13 0.15 21
DS -1 -13 -2 -13 0.15 21
DS -2 -13 -2 -14.5 0.15 21
DS -2 -14.5 -3 -14.5 0.15 21
DS -3 -14.5 -3 -13 0.15 21
DS -3 -13 -5 -13 0.15 21
DS -5.5 -8 -5.5 -8.5 0.15 21
DS -5.5 -8 -5.5 11 0.15 21
DS -5.5 11 5 11 0.15 21
DS 5 8.5 5 -15 0.15 21
DS 5 -15 -5.5 -15 0.15 21
DS -5.5 -15 -5.5 -11 0.15 21
$PAD
Sh "2" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 -6
$EndPAD
$PAD
Sh "3" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 -4.5
$EndPAD
$PAD
Sh "4" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 -3
$EndPAD
$PAD
Sh "5" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 -1.5
$EndPAD
$PAD
Sh "6" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 0
$EndPAD
$PAD
Sh "7" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 1.5
$EndPAD
$PAD
Sh "8" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 3
$EndPAD
$PAD
Sh "9" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 4.5
$EndPAD
$PAD
Sh "10" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 6
$EndPAD
$PAD
Sh "11" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 7.5
$EndPAD
$PAD
Sh "12" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 9
$EndPAD
$PAD
Sh "13" O 1.5 1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 10.5
$EndPAD
$PAD
Sh "14" O 1 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -5.5 12
$EndPAD
$PAD
Sh "15" O 1 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -4 12
$EndPAD
$PAD
Sh "16" O 1 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -2.5 12
$EndPAD
$PAD
Sh "17" O 1 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1 12
$EndPAD
$PAD
Sh "18" O 1 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 0.5 12
$EndPAD
$PAD
Sh "19" O 1 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 2 12
$EndPAD
$PAD
Sh "20" O 1 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 3.5 12
$EndPAD
$PAD
Sh "21" O 1 1.5 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 5 12
$EndPAD
$PAD
Sh "22" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 10.5
$EndPAD
$PAD
Sh "23" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 9
$EndPAD
$PAD
Sh "24" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 7.5
$EndPAD
$PAD
Sh "25" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 6
$EndPAD
$PAD
Sh "26" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 4.5
$EndPAD
$PAD
Sh "27" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 3
$EndPAD
$PAD
Sh "28" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 1.5
$EndPAD
$PAD
Sh "29" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 0
$EndPAD
$PAD
Sh "30" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 -1.5
$EndPAD
$PAD
Sh "31" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 -3
$EndPAD
$PAD
Sh "32" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 -4.5
$EndPAD
$PAD
Sh "33" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 -6
$EndPAD
$PAD
Sh "34" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 6 -7.5
$EndPAD
$PAD
Sh "1" O 1.5 1.1 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -6.5 -7.5
$EndPAD
$EndMODULE HC-06
$MODULE WL-SFTW-SMD
Po 0 0 0 15 543E4170 00000000 ~~
Li WL-SFTW-SMD
Sc 0
AR /53C39C9E
Op 0 0 0
T0 0 2.54 1 1 0 0.15 N V 21 N "L1"
T1 0 -2.54 1 1 0 0.15 N V 21 N "RGB_LED"
DS -2.032 -1.524 -1.778 -1.778 0.15 21
DS -1.778 -1.778 2.032 -1.778 0.15 21
DS 2.032 -1.778 2.032 -1.524 0.15 21
DS -2.032 1.524 -2.032 1.778 0.15 21
DS -2.032 1.778 2.032 1.778 0.15 21
DS 2.032 1.778 2.032 1.524 0.15 21
$PAD
Sh "1" R 1.5 0.9 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.55 -0.85
$EndPAD
$PAD
Sh "2" R 1.5 0.9 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po 1.55 0.85
$EndPAD
$PAD
Sh "3" R 1.5 0.9 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.55 -0.85
$EndPAD
$PAD
Sh "4" R 1.5 0.9 0 0 0
Dr 0 0 0
At SMD N 00888000
Ne 0 ""
Po -1.55 0.85
$EndPAD
$EndMODULE WL-SFTW-SMD
$EndLIBRARY
